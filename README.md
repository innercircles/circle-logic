<div align="center">

![innercircles](innercircles-logo-square.png)

</div>

# Circle logic

In circle logic we elaborate how our core principles can be translated to comprehensive, practical and easy-to-understand governance policies in ways that are a natural extension to the life philosophy we adopt and improve.

## Contents

- [Introduction](#introduction)
- [Conduct](#conduct)
- [Privacy](#privacy)
- [Participation](#participation)
- [Contribution](#contribution)
- [Collaboration](#collaboration)
- [License](#license)

## Introduction

In innercircles we state there are no rules to our cooperation as circle builders, other than alignment to our principles. No rules because one of those principles is the intrinsic value of Freedom, and the principles as a whole represent a revaluation of commonsense human values and humanity in all our relationships and interactions with others. We redefine human value to its original intent and meaning, where modern society has warped and twisted them.

Our objective is to show people how rewarding our simple life philosophy is. Not by preaching it, but by the gradual adoption that follows automatically through our participation, interaction and collaboration. To demonstrate how people can take more control of their life and steer towards their dreams. Showing how by taking the initiative and actively seeking for opportunities we can set out on a path towards an _evolutionary lifestyle_ and more flourishing life.

But we can't do without rules entirely. We need both organization structure and governance policies in order for us to scale, as well as fallbacks to more formal regulation to deal with issues that arise in any complex social structure. This project will develop these rules and procedures and continually refine and improve them.

## Conduct

Most online communities have a formal Code of Conduct that defines desired behaviour and handholds to deal with conflicts. Here we develop ours.

- [Code of Conduct design](code-of-conduct/README.md)

## Privacy

We consider privacy a fundamental human right. Our privacy policies must reflect that, and in ways that do without incomprehensible legalese.

## Participation

Where others define an official Terms of Use, we are seeking to define what Participation entails. One of our requirements is that all participation is voluntary and should be "fun" and rewarding.

## Contribution

Where can you jump in and contribute, and what happens to your contributions? Here we talk about copyright and licenses, but also how you can reap maximum benefit from your contributions.

## Collaboration

How do we organize most efficiently as we grow, and keep our organization manageable? We develop governance policies to foster sustainable growth and productivity.

## License

Copyright ⓒ 2020 smallcircle foundation. All content in this repository is released under [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/.)