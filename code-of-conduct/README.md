# Code of conduct

In this section we develop a Code of Conduct for innercircles that is hopefully less formal than CoC's usually are, and which is tailored to our life philosophy.

## Contents

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Exemplars](#exemplars)
- [Elaboration](#elaboration)

## Introduction

The life philosophy we develop in innercircles is targeted to finding and gradually adopting an Evolutionary Lifestyle - one that allows us to progress towards a brighter future. For this purpose we engage in a rediscovery process of _true_ human values - which are eroding by forces of modern society - and showing the intrinsic rewards of doing so. 

Our life philosophy eventually needs not to be taught, as it is based on common sense and natural principles of Freedom and Happiness that apply to anyone. From this perspective we state that for any person to participate in our circles there are **No Rules** other than respecting human values, freedom and humanity.

In theory this should work well, and our methodology entails that anyone not doing so would not reap the benefits of their participation, and 'push themselves out of the circle' by their undesirable behavior. In practice though we need a fallback to more formal rules, to handle real conflicts that arise and for people that truly misbehave. We must documentat clear guidance and procedures on dealing with that.

## Requirements

The Code of Conduct should adhere to the following requirements:

- [**Brevity**](#brevity): Should be as brief as possible, but still be complete.
- [**Clarity**](#clarity): Should be as clear as possible, written in understandable human language.
- [**Commonsense**](#commonsense): Should be a natural extension to innercircles principles.
- [**Universal**](#universal): Should be applicable to the full range of innercircles tools and projects.

#### Brevity

Most code of conducts are long texts, or have a lot of cross-references to related documentation that make them long. The reason is to make them complete and proper tools to handle all conflicts that can occur. The downside to this is that they are often not read upfront by the people to whom they apply. They only pop up after a conflict has started and reasonable arguments are no longer enough.

#### Clarity

The texts of a code of conduct should cover conduct in ways that leave no room for misunderstandings and different interpretations. This is extremely difficult, especially when combining with the first requirement of Brevity. The use of natural, easy-to-understand sentences and common words is important. Oftentimes code of conducts read more like formal legalese, and this should be avoided.

#### Commonsense

The code of conduct should be an appeal to remember what it means to be a good human being, and treat others in the same way that we would like to be treated ourselves. In that way it is a natural extension to innercircles principles, where this notion is implicitly present. Most code of conducts deal with this quite well, but we can look on honing the texts further to align better with innercircles philosophy.

#### Universal

As innercircles grows and extends her activities we'll use more and more different platforms, tools and facilitate a wide-ranging host of projects and initiatives. The core of our code of conduct should be applicable to all of these, while for individual applications additional rules and procedures may be in order. The universal applicability of our Code of Conduct also means that we can make it available for others to reuse under a permissive license.

### Technical requirements

When presented as a HTML page:

- **Accessibility**: The document must adhere to [WCAG](https://www.w3.org/WAI/standards-guidelines/wcag/) guidelines.
- **No-script**: The document uses no Javascript for dynamic features or trackers.
- **Open license**: The document is provided under an open Creative Commons license.
- **Mobile support**: The document must display well on mobile devices. 

## Exemplars

We will build on the shoulders of giants. Below are a number of Code of Conduct examplars that we'll analyse as input for our work.

- [Contributor Covenant Code of Conduct](../exemplars/contributor-covenant/README.md)
- [Snowdrift.coop Code of Conduct](../exemplars/snowdrift/README.md)
- [Greaterthanlearning Relationship Agreement](../exemplars/greaterthanlearning/README.md)
- [Discourse FAQ](../exemplars/discourse/README.md)
- [Other sources of inspiration](../exemplars/miscellaneous/README.md)

## Elaboration

#### Content draft

The following document is where content is aggregated for inclusion in the final document:

- Proof of concept: [Participation Guidelines (Terms)](participation-guidelines-poc.md)

#### Document name

We choose the name: **Participation Guidelines**<br>
In hyperlinks we use: **Participation Guidelines (Terms)**

<details><summary>Read the rationale (click to expand)</summary>

- The name _code of conduct_ is inappropriate. In life there's no written code either. It is implicit.
- In innercircles things revolve around 'being part of our movement', hence _participation_ is appropriate.
- An _agreement_ is too formal. We are guided through life by luck and circumstance, so we use _guidelines_.

_Guidelines_ have the appropriate meaning to express rules and procedures:

> [**guideline**](https://www.thefreedictionary.com/guideline) (gīd′līn′)
>
> A statement or other indication of policy or procedure by which to determine a course of action.
>
> - guidance, counsel, direction - something that provides direction or advice as to a decision or course of action.
> - rule, regulation - a principle or condition that customarily governs behavior.
>
> _(source: [The Free Dictionary](https://www.thefreedictionary.com))_

To clarify that this refers to official documentation, in hyperlinks we should use the text _"Relationship Guidelines (Terms)"_, same as Greaterthanlearning does for their [Relationship Agreement](exemplars/greaterthanlearning/README.md).

</details>
<br>

#### Document structure

Guidelines reflect our: **Core principles and Intrinsic values**<br>
We categorize by principles: **think, act, feel, play**<br>
Each guideline relates to a: **Human value or quality** 

<details><summary>Read how this works (click to expand)</summary>

In innercircles we have the following core principles:

- **Mindfulness** (think): Common knowledge/wisdom/sense. Think before acting. Only progress counts.
- **Sustainability** (act): Doing, taking initiative, collaboration, dreams provide guidance.
- **Happiness** (feel): Reflect on thought and actions. Emphasize on human value, emotions. Dare to feel.
- **Playfulness** (play): Learn/teach by play. Personal/collective growth. Fun. Unleash creativity.

Our intrinsic values are:

- **Freedom**: Freedom of mind/spirit/choice/expression as human rights. All participation is voluntary.
- **Humanity**: Human values, dignity, equality. Things that bind us. Peace and harmony. Culture.

The intrinsic values need not be explicitly mentioned: they are intrinsic, a given. The CoC rules and procedures should be related to core principles, so we use these to categorize them.

</details>
<br>

