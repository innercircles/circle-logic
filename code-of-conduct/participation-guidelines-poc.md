# Participation guidelines

> **Note**: This proof of concept evaluates how [design choices](README.md#elaboration) match actual rules and procedures that should be part of our code of conduct. The final document will be derived from content presented here.

## Guidelines

<table width="100%">
  <thead>
    <tr>
      <th width="15%">Title</th>
      <th width="5%">Category</th>
      <th width="50%">Contents</th>
      <th width="30%">Remarks</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><b>Mindful</b></td>
      <td>think</td>
      <td>
        <ul>
          <li>We treat others with dignity and compassion.</li>
          <li>We interact in ways that are thoughtful and substantive.</li>
        </ul>
      </td>
      <td>
        <ul>
          <li>Inspired by <a href="../exemplars/snowdrift/README.md">Snowdrift</a>, <a href="../exemplars/miscellaneous/README.md#hacker-news-guidelines">Hacker News</a></li>.
        </ul>
      </td>
    </tr>
  </tbody>
</table>

