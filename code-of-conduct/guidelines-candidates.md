# Candidate guidelines

This documents lists conduct elements that we would like to see represented in our Participation Guidelines.

## Candidates

The following statements from the [Snowdrift Code of Conduct](../exemplars/snowdrift/conduct.md):

- [ ] I will participate with honesty and good faith.
- [ ] I will honor our community values.
- [ ] I will treat others with dignity and compassion.
- [ ] I will respect others’ privacy.
- [ ] I will criticize constructively.
- [ ] I will take constructive criticism gracefully.
- [ ] I will avoid patterns that escalate conflicts or promote hysteria.
- [ ] I will respect discussion topics and not derail conversations.
- [ ] I will fix my mistakes.
- [ ] I will help others fix their mistakes.
- [ ] Take action any time you see even minor violations of this Code of Conduct.

---

These statements from the [Greaterthanlearning Relationship Agreement](../exemplars/greaterthanlearning/README.md):

> **Note**: Service Provider role is not applicable to innercircles until we have an incorporated foundation at the center of the movement.

Community guidelines:

- [ ] Connect with other participants and content. Help build a shared vision of our collective future
- [ ] Collaborate with fellow participants, the >X team and your own organisation. We can learn faster together
- [ ] Commit to the actions you choose to take. Be the change you seek in the world
- [ ] Embrace ambiguity, celebrate your victories and own the consequences of your “happy little accidents”


Greaterthanlearning, Community Facilitator, Can Do:

- [ ] Provide feedback to community members when they share their progress
- [ ] Suspend your membership in the community if: 
    - [ ] you repeatedly contradict the community guidelines through your actions
    - [ ] you behave in an offensive manner to any community member or one of our staff
- [ ] Issue you with a warning that you are breaking our community guidelines by:
    - [ ] Sending an explicit warning to your registered email within 36hrs of us observing, or being informed of your actions

Greaterthanlearning, Community Facilitator, Can't Do:

- [ ] Suspend your membership without warning 
- [ ] Give you immediate feedback on course progress. We have lives too

Greaterthanlearning, Course Creators, Can't Do:

- [ ] Be across every possible aspect of research that has been done in the subjects and fields we cover in our courses. We are flawed homo sapiens. But we are willing to learn, evolve and attempt to better ourselves

Users, Community Member, Can Do:

- [ ] Connect with other community members, build relationships with those members and collaborate on projects together
- [ ] Share your thoughts and ideas openly about how to improve our service, courses, learning experiences, and overall platform
- [ ] Take what you’ve learned and put it to the test in your own business or personal projects. In fact, this is what we really want people to do

Users, Community Member, Can't Do:

- [ ] Use the community as a way to market and sell your services unless people explicitly ask you to
- [ ] Be abusive to other community members
- [ ] Invite people via built-in platform features that you believe might knowingly try to undermine our community values
- [ ] Take models, methods and tools you learn from content creators and claim them as your own for commercial purposes. There’s a bit more on this in the Intellectual Property and Copyrights section below

Users, Website Visitor, Can't Do:

- [ ] Duplicate our website content and claim it as your own

Disclaimers:

- [ ] This is a learning platform (You own the consequences of applying the knowledge you acquired)
- [ ] Our platform will evolve (Things may change without your prior knowledge. We document our changes)
- [ ] We're mere mortals (We don't know the future, cover every scenario. Give us your feedback)