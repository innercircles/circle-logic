<div align="center">

[![Greaterthanlearning](https://codeberg.org/innercircles/circle-logic/raw/branch/main/exemplars/greaterthanlearning/logo_with_name_vertical_01-400x205.png)](https://greaterthanlearning.online)

# [The World's First Social Learning Platform <br>For Ethical Change Makers](https://greaterthanlearning.online)

</div>

## Greaterthanlearning Relationship Agreement

#### CoC document name

The name 'Relationship Agreement' is much friendlier than Code of Conduct. For innercircles it is still too formal. We don't agree to be part of life, but we life guide us. We use _Participation Guidelines_ as our terminology.

#### Overall structure

The agreement is a combination of Code of Conduct and Terms of Service in one document, structured as:

- Short intro
- Purpose, values and principle
- Relationship roles & responsibilities divided in 'Ours' and 'Yours'
  - Ours as Service Provider
  - Ours as Community Facilitator
  - Ours as Course Creator
  - Yours as Community Member
  - Yours as Website Visitor
- Business model
- IP and Copyrights
- Your Rights
- Governance
- Disclaimers

#### Values and core principle

The Greaterthanlearning platform provides a [Relationship Agreement](https://greaterthanlearning.online/relationship-agreement/) that serves as a combined Terms of Service and Code of Conduct document. It is formatted in a very user-friendly, easy-to-read manner and does not use complex terminology.

Summarizing from the agreement, here are the Greaterthanlearning core values, and the single principle they highlight (as specifically relevant to the agreement):

| Value | Relation to innercircles concepts |
| :---: | :--- |
| **Connection** | Aligns to our _"Come together"_ vision statement. Support each other, share ideas, improve the world. |
| **Collaboration** | Aligns to _"Come fullcircle"_ in our vision, _People profit_ methodology and _Playfulness_ principle. |
| **Commitment** | _Mindfulness_ principle and _Freedom_ intrinsic value dictate commitment is not needed. But _People profit_ highlights the value of initiative to create opportunities. |
| **Consequence** | Relates to our virtuous cycle of mutualism and synergy, and how _thinking_ and _acting_ relates to _feeling_. |

Their core principle is: **Preference is greater than Acceptance** and is defined as follows:

> Well, if we assume that ethics is the process of making a decision that best aligns to our purpose, values and principles (as The Ethics Centre proposes), then ethics is process driven. It’s about optimising for the most socially preferable outcomes. It’s not about justifying social acceptance or what we can get away with.
>
> This – an intense focus on social preferability – is our north star. It helps us define what we should and shouldn’t do. It’s the way we go about determining right from wrong.
>
> It’s important to note that we don’t make these decisions in a silo. We execute social preferability testing. We involve you, our community, in the process.

This definition - though innercircles aligns with it - is too abstract and difficult to understand. Some observations:

- _Ethics is process driven_: This a very nice concept and way of thinking about Ethics.
- _Ethics aligning to purpose, values and principles_: This is where ethics get incredibly complex to define when scaling to the whole of society. In innercircles we focus on how various aspects of life apply to us all, and avoid defining this in terms of Ethics.
- _Social preferability_: This concept is probably easily misunderstood as 'conforming to the norm' rather than accepting that everyone is unique and different. Taling about 'social cohesion' may be better. Seeking 'that which binds us'.
- _Social preferability testing_: Is too technical terminology. Finding how social cohesion works best is a continual discovery process, and everyone is involved.
- _Involve our community_: This is key and important, but the way it is phrased places Greaterthanlearning above the community, not an equal party that is part of it.

There's also mention of the technical term _Designing our relationship_. We'd rather talk about _Evolving/Fostering/Nourishing our relationship_. Maybe 'Nourishing' works best.

#### Community guidelines

The guidelines for members to follow flow directly from the Community Values in the relationship agreement, and are brief as can be:

> 1. **Connect** with other participants and content. Help build a shared vision of our collective future
> 2. **Collaborate** with fellow participants, the >X team and your own organisation. We can learn faster together
> 3. **Commit** to the actions you choose to take. Be the change you seek in the world
> 4. Embrace ambiguity, celebrate your victories and **own the consequences** of your “happy little accidents”


#### Roles and responsibilities

This section is very interesting and provides easy-to-understand clarity on:

- "What you can expect from us"
- "What we expect from you"

The division by role separates things nicely. Most interesting is that new members are taken through all of this in a paced-down manner during the onboarding process. This makes it much more likely that people actually read it.

Greatherthanlearning assigns the following roles to themselves:

- Service Provider
- Community Facilitator
- Course Creator

And the following roles to users:

- Community Member
- Website Visitor

In innercircles we may also divide into roles and/or provide additional addendums depending on the tool being used (forum, code repository, chatroom, etc.)

#### Disclaimers

Three disclaimers are mentioned:

1. **This is a learning platform**: You own the consequences of applying the knowledge you acquired.
2. **Our platform will evolve**: Things may change without your prior knowledge. We document our changes.
3. **We're mere mortals**: We don't know the future, cover every scenario. Give us your feedback.