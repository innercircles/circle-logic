# Miscellaneous resources

This document highlights various other resources that are inspirational for the formal documents we create.

## Hacker News Guidelines

[Hacker News](https://news.ycombinator.com) is the social network for tech-minded people, that is affiliated to the YCombinator startup incubator in Silicon Valley. Members submit resources which can be commented on, and both submissions and comments are upvoted by other members. The site is used by techies from all around the world, and has real high quality comment threads.

The beauty is that the entire network is incredibly well-moderated by just 2 persons, one active ([@dang](https://news.ycombinator.com/user?id=dang)) and one in the background. In this task they often refer to the [Hacker News Guidelines](https://news.ycombinator.com/newsguidelines.html). This document has been thoroughly battle-tested.

Here are some highlights and best-practices from the guidelines (quoting from the document itself):

- Mentions what is on-topic and off-topic:
  - On-topic is _"Anything that good hackers would find interesting [..] anything that gratifies one's intellectual curiosity."_
  - Off-topic are _"Most stories about politics, or crime, or sports, unless they're evidence of some interesting new phenomenon."_
- _"Comments should get more thoughtful and substantive, not less, as a topic gets more divisive."_
- _"When disagreeing, please reply to the argument instead of calling names."_
- _"Please respond to the strongest plausible interpretation of what someone says, not a weaker one that's easier to criticize. Assume good faith."_
- _"Eschew flamebait. Don't introduce flamewar topics unless you have something genuinely new to say. Avoid unrelated controversies and generic tangents."_
- _"Please don't post shallow dismissals, especially of other people's work. A good critical comment teaches us something."_
- _"Please don't use Hacker News for political or ideological battle. It tramples curiosity."_
- _"Please don't comment on whether someone read an article. 'Did you even read the article? It mentions that' can be shortened to 'The article mentions that.'"_
- _"Please don't post insinuations about astroturfing, shilling, brigading, foreign agents and the like. It degrades discussion and is usually mistaken."_
- _"Please don't complain that a submission is inappropriate. If a story is spam or off-topic, flag it. Don't feed egregious comments by replying; flag them instead. If you flag, please don't also comment that you did."_

The formatting as a request with "Please.." instead of a commandment is a friendly way to appeal to humanity and commonsense.

## Conscious Leadership Group

On the [conscious.is](https://conscious.is/) website the reader is presented with a roadmap that onboards them via several short videos, and ends with the presentation of 15 info cards that are the commitments for conscious leaders.

The cards are formatted as follows:

- A single-word title that defines the commitment.
- A single-sentence "by me" that details the pattern to follow: "I commit to...".
- A single-sentence "to me" that provides the anti-pattern on how NOT to be a conscious leader.