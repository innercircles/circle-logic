# What Does *Free/Libre/Open* Mean?

categories: communications
...

Snowdrift.coop supports projects that respect **freedoms of access, use, modification, and sharing.** What concise term should we use to refer to these types of works?

## Confusion around the English word "free"

In English, **"free" often means *free of charge*; but, in other contexts, "free" means *freedom***. To clarify, some people say "free as in speech not free as in beer" or simply "free as in freedom".

The lack of clarity for the term "free software" makes for an ineffective search query. "Free culture" is only slightly better. Terms like "software freedom" and "cultural freedom" offer improved clarity but only work in certain grammatical contexts.

### "Libre" to clarify freedom

Borrowing from Romance languages, the adjective *libre* (as in liberty) contrasts with *gratis* (zero price). For example, the most popular free-as-in-freedom office suite is called [LibreOffice](http://libreoffice.org/). At this time, "libre" remains mostly foreign to the English-speaking world.

## "Free/libre" vs "open"

**The term *open* emphasizes access, transparency, and inclusive collaboration.** We value these qualities and have designed Snowdrift.coop to promote expanded access, honest communication, and maximum participation.

On its own, "open" can be vague. Open *Access* works may have restrictions on use, modification, or sharing. Today we see widespread open-washing where the term "open" gets thrown around to sound nice while the products remain restricted. The now-common term "Open Source" also has a problematic software-centric framing^["Source" usually refers to "source code" rather than other sorts of source for other works. Also quite unfortunately, "software" has come to refer only to executable computer programs rather than anything *soft*, i.e. anything that isn't hardware.] and focuses mainly on the development process rather than on the overall ethics of a project.

Overall, **"open" tends to complement rather than replace "free/libre"**. Freedom on its own doesn't emphasize collaboration, participation, or transparency. Openness on its own does not necessarily bring freedom (especially when "open" refers only the source and not to the final product). Real freedom also requires respect for privacy, and the term "open" doesn't really address that concern. At Snowdrift.coop, we care about both open participation *and* essential liberties.

### Competing terms and organizations

The software world has had long-running and contentious debates about these terms. The [Open Source Initiative](http://opensource.org/) (OSI) and the [Free Software Foundation](http://fsf.org/)[^FSF] have nearly synonymous criteria for their respective software definitions, but they have distinct political perspectives. The OSI emphasizes the practical and technical benefits of collaborative development, while the FSF focuses on the ethics of freedom and liberty.

[^FSF]: The Free Software Foundation would be better renamed as the "Software Freedom Foundation" or the "Foundation for Software Freedom"

In working to appeal to business interests, the OSI has a history of actively downplaying the ethical philosophies espoused by the FSF. So, the FSF asks people to avoid the terms "open" and "open source".[^GNU]
In fact, **many (perhaps most) "Open Source" advocates consider it perfectly normal and acceptable to use "Open Source" software to build proprietary software; whereas advocates for "software freedom" consider the publishing of proprietary software to be an immoral act that harms society.** Thankfully, the OSI has moved toward more alignment with FSF lately, and, as of this writing, the majority of OSI board members are members and supporters of the FSF.

[^GNU]: See the gnu.org article [*Why Open Source misses the point of Free Software*](http://www.gnu.org/philosophy/open-source-misses-the-point.html)

At Snowdrift.coop, we support the FSF's mission to rid the world of proprietary software, but we're not convinced about the tactic of demonizing the term "open". We focus on providing a better [economy for non-scarce goods](https://wiki.snowdrift.coop/about/economics) in order to remove the economic excuse for proprietary restrictions.

Beyond software, these competing terms have carried over into other media, as in the [Free Culture Foundation](http://freeculture.org/) and [Open Knowledge](http://okfn.org). These organizations have different emphases but work toward similar goals. Thankfully, Open Knowledge has an ["Open Definition"](http://opendefinition.org/od/) that explicitly acknowledges "Free/Libre" as synonymous and aligned. A great number of other competing definitions make use of the word "open" in various ways.[^FreeLibre]

[^FreeLibre]: Uses of these terms fall all over the spectrum. The term "[Open Educational Resources](https://en.wikipedia.org/wiki/Open_educational_resources)" (OER) has gained wide recognition, but many different institutions claim definitions that range from entirely free/libre to anything accessible at no charge but otherwise restricted. The relatively obscure term ["open-by-rule"](http://webmink.com/essays/open-by-rule/) implies freedom, equality, and openness; and it goes beyond the license itself towards a more ethical and honorable overall environment. The "[Open Content](http://opencontent.org/definition/)" definition covers all the free/libre values pretty well with just a different framing. On the opposite extreme, an example of open-washing (some say "fauxpen source"): [Binpress](http://www.binpress.com) promotes what they call "open source" software which includes proprietary restrictions on what the licensees may do with the source (such as no redistribution), thus blatantly violating the OSI's definition.

## FLO

As no single choice covers all our values clearly, **the best solution acknowledges all the terms via the combination *free/libre/open* or *FLO*.** We see this commonly used as part of the acronym FLOSS: Free/Libre Open Source Software. Of course, besides software, Snowdrift.coop supports art, music, education, science and more. So, "FLO" fits our purposes better than any alternative term.[^FSF-neutral]

[^FSF-neutral]: The FSF agrees on "FLO" and "FLOSS" as the best neutral terms, but they still complain that even acknowledging the term "open" [downplays their message of freedom](http://www.gnu.org/philosophy/floss-and-foss.html). We do not agree with that zero-sum view of the situation. Celebrating openness need not detract from our focus on freedom. We don't use a combined term in order to stay neutral. We inclusively *affirm* both the values of openness and of freedom.

Besides being inclusive, "FLO" provides value as a novel term. As common words, "Free" and "Open" have many interpretations in different contexts. No attempt to prescribe strict use of those terms will ever succeed. In contrast, "FLO" has no meaning other than the one we intend.[^FloMeaning]

[^FlowMeaning]: The novelty and searchability of "Open Source" is one significant reason that it has achieved greater adoption and overall success over "Free". The OSI actually once had a trademark on "Open Source" but let it expire.

A poetic interpretation of FLO emphasizes the *flow* of ideas, culture, creativity, and liberties that are possible when we remove artificial restrictions and encourage cooperation.[^Flow]

[^Flow]: We noticed only after we'd written it that our slogan at the time, "clearing the path to a Free/Libre/Open world", actually provides the full acronym FLOW, which seems appropriate although perhaps a little too cutesy to reference often. FLOW could also be FLO Work(s), and while that makes sense, we've found it grammatically odd in context. On the other hand, Joseph Potvin has embraced this acronym fully and promotes the idea of [FLOW versus RENT](http://osi.xwiki.com/bin/Projects/About+FLOW+and+RENT+Relationships) as in Free/Libre/Open Works versus Restricted Exclusive Negotiated Titles.

Given the need for clarification in any case, if using "FLO" or "free/libre/open" causes people to ask what we mean, then we have a chance to provide our answer.[^FlowGlossary]

[^FlowGlossary]: For a really thorough discussion of the terminology, history, and specifications about FLO and related terms, see [A Free, Libre, Open Glossary](http://livinglibre1.wordpress.com/2013/08/13/a-free-libre-and-open-glossary-2/) from Living Libre.

*Note: for further clarity about wordings here at Snowdrift.coop, see our [terminology page](https://wiki.snowdrift.coop/about/terminology).*

## Other alternative terms

Various people have suggested terms referencing "public" as in "public software" or "commons" as in the term "digital commons" or the organization [Creative Commons](https://creativecommons.org). These terms have their own challenges either in lacking clarity or in lacking grammatical flexibility. We use these terms in various contexts but not as our definitive term for the class of projects we support.

## How to recognize FLO works

The simplest guarantee: **Everything on Snowdrift.coop is FLO**. For clarity about what does or doesn't meet the definition of FLO, see our page [*Why Only Free/Libre/Open Projects*](why-flo.md).

Other reliable all-FLO collections exist. Unlike most software-centric projects, [Debian GNU/Linux official packages](https://www.debian.org/distrib/packages) are all strictly FLO, even when it comes to non-software items,[^DebianGuidelines] and the same is true for the [gNewSense GNU/Linux distribution](http://www.gnewsense.org/).

[^DebianGuidelines]: The Debian Free Software Guidelines are the original source for the nearly identical Open Source Definition.

For software, the FSF has a [**Free Software Directory**](http://directory.fsf.org/wiki/Main_Page) and Wikipedia (which is itself FLO) has many lists of software on appropriate category pages, often (but not always) with clear indications of proprietary vs FLO.[^Directories] [Prism-break.org](https://prism-break.org) is a superb guide to privacy- and freedom-respecting software and services specifically for internet/communications purposes where suggestions are fully FLO whenever possible. Outside of software, the best directory is the [Free & Open Works wiki](http://faow.referata.com), a listing of FLO resources in many different areas (along with some shareable non-FLO listings that are clearly indicated as non-FLO).

[^Directories]: Both the FSF and Wikipedia directories are incomplete and welcome volunteers to assist in improving them. Although other directories exist, the FSF and Wikipedia directories are the only ones we know of that are both robust and are themselves fully FLO. Some proprietary directories that are nonetheless useful include [freecode.com](http://freecode.com/) which lists mostly *but not exclusively* FLO software and the high-quality site [alternativeto.net](http://alternativeto.net/) which lists both FLO and proprietary software and uses the term "Open Source" for FLO and the term "Free" to mean free-of-charge-but-proprietary.

In general, "free" often means only price, so look for clarifying statements such as "free as in freedom" or "free/libre". Resources called "open" and "open source" are often legitimately FLO but can also be harder to verify without checking the exact license. Among the many licenses, it may help to recognize the most common FLO ones. For software the most common FLO licenses are probably MIT and GPL (also popular: Apache, BSD, AGPL). For other works, CC0, CC-BY, and CC-BY-SA are the three FLO options from Creative Commons. See our [license discussion](licenses.md) for more information.
