# Snowdrift.coop Values and Principles

categories: community, conduct
...

# Snowdrift.coop Values and Principles

The ideals described here guide and frame our specific practices and decisions in various contexts around Snowdrift.coop including our [Code of Conduct](conduct.md), governance, and more.

## Free/Libre/Open (FLO)

Our [mission](https://wiki.snowdrift.coop/about/mission) focuses on funding FLO public goods. The associated vision and aims emphasize further principles in achieving the mission.

As described in our [explanation of the term FLO](free-libre-open.md), we believe that ideas, culture, and technology should be freely accessed, used, adapted, and shared. In developing such resources, we support open transparency and inclusive collaboration.

Our guide to [honorable project ideals](honor-projects.md) considers these and other related values.


## Co-op values, ethics, and principles

Snowdrift.coop, as a cooperative organization, honors the [co-op values](https://www.ica.coop/en/whats-co-op/co-operative-identity-values-principles) of **self-help**, **self-responsibility**, **democracy**, **equality**, **equity**, and **solidarity**. Co-op members accept the ethics of **honesty**, **openness**, **social responsibility**, and **caring for others**.

We also follow the seven Co-op Principles:

1. Voluntary and Open Membership
2. Democratic Member Control
3. Member Economic Participation
4. Autonomy and Independence
5. Education, Training and Information
6. Cooperation among Co-operatives
7. Concern for Community


## Celebrating Diversity

Great value comes out of the interactions of different people and perspectives.
So, we aim to **maximize diversity** in our inclusive community and accept the inherent challenges that brings.

We live within a context far from ideal. Distributions of power and privilege remain extremely unequal. Systemic discriminations favor some personal backgrounds, characteristics, and identities over others. Instead of constructive engagement between people of different perspectives, many trends (especially online) show growing segregation, groupthink, and echo-chambers.

To achieve and maintain the healthy, diverse community that we want, we give appropriate deference to the needs and concerns of those from marginalized, underprivileged, or otherwise underrepresented groups. We also give appropriate deference to experts. Regardless, we treat all individuals with dignity and believe that everyone deserves the chance to be heard.

We aim to welcome and include people with different:

- ages
- genders / sexes / sexual orientations
- races / ethnicities / national origins
- geographic locations
- native and secondary languages
- neurotypes
- abilities (including mental or physical disabilities or special needs)
- socioeconomic status / class
- cultures, subcultures, and lifestyles
- family structures
- educational experiences
- professions
- skills and interests
- religious and political views — as long as they respect our core values

In addition to the our [Code of Conduct](conduct.md) which prohibits bigotry and so on, we have a supplementary [healthy communication guide](honor-users.md) with further best-practices for sustaining diverse and robust communities.

## Restorative and Transformative Justice

In resolving any conduct violation or interpersonal conflict, we aim to *restore* all parties to good standing whenever feasible. We work to motivate the best behavior through honor rather than shame. We focus on responsibility rather than blame.

When anyone feels harmed, we offer them an empowered role in negotiating an outcome that will allow them to find sincere forgiveness. Instead of humiliating offenders, we help them to recognize harms and take responsibility.

We also aim for *Transformative Justice* which means considering the broader contexts (such as power differentials and systemic discrimination). In resolving any tensions, we ask what constructive changes we could make on a systemic level.

A great summary comes from the [Zehr Institute for Restorative Justice](http://www.zehr-institute.org/resources/restorative-or-transformative-justice):^[For in-depth perspectives on the relationship and differences between Restorative Justice and Transformative Justice, see [*An Overview of the History and Theory of Transformative Justice*](http://www.review.upeace.org/pdf.cfm?articulo=124) from the Peace & Conflict Review.]


> ### Retributive Approach
> [typically dysfunctional and dehumanizing]
>
> The incident is a violation of the policies, defined by rule breaking. Resolution involves looking at the incident, determining blame, and administering the consequences.
>
> - What rule has been broken?
> - Who is to blame?
> - What punishment do they deserve?
>
> ### Restorative Approach
>
> The incident is a violation of people and relationships. It creates obligations to make things right. Resolution involves looking at the harm caused by the incident: harm to the person(s) who were victimized, harm to the instigator/aggressor(s), and harm to the larger community; and asks “How can this harm be repaired?”
>
> - Who has been hurt & what are their needs?
> - Who is obligated to address these needs?
> - Who has a “stake” in this situation & what is the process to involve them in making things right and preventing future occurrences?
>
> ### Transformative Approach
>
> The incident may have occurred as a result, in part, of unhealthy relationships and social systems.  It creates obligations to build new or better relationships. This must happen not only at an individual level but at the level of social structures and institutional policies. Resolution involves changing wider social systems in ways that help to prevent the occurrence and re-occurrence of harmful incidents.
>
> - What social circumstances promoted the harmful behavior?
> - What structural similarities exist between this incident and others like it?
> - What measures could prevent future occurrences?
