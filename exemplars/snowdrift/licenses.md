# FLO Licensing Discussion

categories: communications
...

This page discusses the issues with different types of FLO licenses.

We have a separate discussion on [*what makes a license FLO*](whyfree#understanding-flo-licenses).

## Categories

* *Copyleft*: derivatives must keep FLO terms[^Copyleft]
* *Permissive*: derivatives may use either FLO or proprietary terms
* *Copyfree*: permissive licenses which minimize restrictive clauses and maximize compatibility

[^Copyleft]: For an extremely thorough discussion of the nature of copyleft and its legal history and details, see [Copyleft.org](http://copyleft.org)

## Our position

Snowdrift.coop emphasizes the importance of cooperating toward mutual goals of freedom, compatibility, and creative productivity. We maximize the standing of FLO in the world by emphasizing ideals while advocating practical strategies.

The main co-founders of Snowdrift.coop support copyleft terms, and the site itself uses copyleft licensing. However, consistent with our dedication to cooperation and inclusion, we have allied with the [Copyfree Initiative](http://copyfree.org/) (whose founder joined our steering committee). We allow projects and users to choose their preferred terms within the scope of general FLO definitions.

## Acceptable FLO licensing clauses

Widely-accepted definitions of FLO (see our [project requirements](project-reqs.md)) accept a couple types of license requirements. Licenses may require specific attribution, such as with CC-BY.[^attribution] Like the GNU GPL, licenses may require that derivatives continue respecting everyone's freedoms. But neither of these requirements are *necessary* for FLO licensing. Importantly, standard FLO definitions *prohibit* restrictive [non-commercial](https://wiki.snowdrift.coop/about/why-free#nc) and [non-derivative](https://wiki.snowdrift.coop/about/why-free#derivatives) clauses.

## Choosing a license

### Copyleft motivations

Richard Stallman invented copyleft licensing to stop proprietary exploitation of FLO software. In his view, proprietary terms treat software users unjustly and proprietary software should never exist at all. He didn't want any of his code to assist the development of proprietary derivatives.

Others may accept the existence of proprietary software but prefer to promote FLO as a better and more ethical direction. In competition between FLO and proprietary programs, permissive licensing lets all the FLO code be exploited by proprietary projects with no counter-balance the other way. A proprietary derivative may sometimes overshadow or effectively co-opt an originally FLO project. So, copyleft terms can help FLO projects stay competitive by blocking any use of their code in proprietary derivatives.

In commercial context, copyleft terms can provide a mutual trust that benefits collaborative businesses. When various institutions use common technological resources, copyleft terms assure that their investments in shared infrastructure will remain in the commons.[^CopyleftLecture] For example, many businesses invest in the Linux kernel because they use the kernel in their work. In contrast, permissively-licensed FLO projects often remain dominated by a single business with a particular vested interest, as in the dysfunctional fragmentation and power imbalance in the permissively-licensed Android system.

[^CopyleftLecture]: See lecture by Eben Moglen [*Copyleft Capitalism: GPLv3 & the Future of Software Innovation*](https://www.youtube.com/watch?v=UneYZikN85Q)

Copyleft terms may increase collaboration and contribution among community authors. Most developers care more about creative goals than about license details or political concerns, so they will use whatever resources fit their needs. Such people might choose proprietary terms for their own derivative projects when allowed but will keep their work FLO when a copyleft license requires it.

Understandably, businesses built on proprietary licensing prefer to avoid discussing the ethics of their terms. To spin things in their favor, they describe copyleft licenses only in terms of how they can help with collaboration, and then they emphasize those cases where *permissive* licenses achieve as much or greater collaboration. So, some license explanations describe copyleft with phrases like "I care about sharing improvements" which has different ramifications than "I care about protecting user freedoms by blocking proprietary restrictions." [^Github]

[^Github]: GitHub, a company that uses a mix of proprietary and permissive licensing (aside from the fact that they are themselves built on a core project, Git, that is copyleft licensed) created the site [choosealicense.com](http://choosealicense.com) which weakly discourages adoption of copyleft licenses by putting permissive as the first default. Their site previously encouraged the use of outdated GPLv2 instead of GPLv3 and described the motivations of copyleft in strictly collaborative terms and not in terms of ethics and freedoms. Thankfully, after pressure from the FSF and others, choosealicense.com fixed some of the issues.

### Copyleft compatibility

To protect FLO terms, Copyleft licenses requiring that derivative works retain the same or similar license. Unfortunately, such clauses can cause incompatibility between FLO resources that could otherwise be combined. To minimize this compatibility problem, we must all stick to using only a standard set of compatible copyleft licenses. For details, see GNU.org's thorough [introduction to license compatibility](https://www.gnu.org/licenses/license-compatibility.html).

So, whether copyleft or not, we *urge* projects to choose terms that are *compatible* with the most common copyleft licenses:

* [GPLv3-compatible software licenses](http://www.gnu.org/licenses/license-list.en.html#GPLCompatibleLicenses)
* CC BY-SA-compatible licenses[^CCCompatible] for most other works
    * In *most* cases, projects that produce work other than executable programs should use one of the three Creative Commons options that are FLO: CC0 (Copyfree), CC-BY (Permissive but not Copyfree), or CC-BY-SA (Copyleft). For details on each of these options, see the [CC license chooser](http://creativecommons.org/choose/).
    * *Note:* [CC BY-SA was determined to be GPLv3-compatible](https://creativecommons.org/weblog/entry/46186).
* Consider providing version flexibility in license terms, such as including an "or later versions"
    * Special notice is not needed with the latest Creative Commons licenses, as such a clause is automatically included.
* Within permissive licenses, those that are *Copyfree* are all compatible both with each other and one-way with *any* other license.

[^CCCompatible]: <https://creativecommons.org/compatiblelicenses>  lists what licenses can be used for derivatives of CC BY-SA works, but we do not know of any comprehensive list of the large number of permissive licenses that are CC BY-SA compatible in terms of allowing allow works to be combined into derivatives that are then licensed CC BY-SA.

### Permissiveness and proprietary derivatives

**The main issue with permissive licenses is that they enable proprietary derivatives.** For our discussion here (and in-line with the Snowdrift.coop [mission](https://wiki.snowdrift.coop/governance/mission)), we'll assume a goal of promoting openness and freedom (as opposed a goal of maximizing profit or business success).

#### Choosing licenses strategically

Because license-incompatibility can harm FLO project development, we can consider whether stopping proprietary exploitation is worth that trade-off. A project may never lead to any derivatives, making the concern irrelevant. Another project may be especially likely to lead to proprietary derivatives unless copyleft terms stop that. A different project may happen to not have that issue but have a real need to be usable and compatible with other FLO projects.

Some people feel that copyleft protections are not worth the risk of any collateral damage to FLO compatibility. They see the best path to FLO success as having the fewest barriers to FLO development, with the hope that FLO just takes over and proprietary terms naturally die away.

On the other hand, proprietary projects remain dominant today. Many trends point to a future filled with partially-FLO products with essential proprietary elements that combine to make final proprietary products. Perhaps we end up with greater FLO balance by blocking proprietary exploitation — even when doing so can have the unfortunate side-effect of incompatible licenses between FLO projects.

The details of each case may determine the best approach. It *might* even help a FLO project's overall success to gain support from commercial interests who wish to create proprietary derivatives [^Proprietary] — as long as this provides a net increase for the FLO community rather than drawing attention away. Certainly, if proprietary interests have enough *proprietary* resources to draw on already, then they *might* dominate the market whether or not they use any FLO resources. In such cases, we may prefer to get such companies to use permissive FLO resources and contribute back as they are willing. The opening up of even a *part* of a proprietary project could be a step in the right direction. On the other hand, there may be cases where the best outcome involves encouraging business use of copyleft licensing.

[^Proprietary]: Note that this is not a redundant statement because *commercial* does not necessarily mean *proprietary*.

The strongest case for copyleft comes when a FLO project is significant and robust enough to out-compete proprietary alternatives and yet would face real risk of proprietary co-option if they used permissive terms. More FLO projects may be in this situation if Snowdrift.coop succeeds at providing them with a greater base of support. Of course, if we also succeed at making it easier for everyone to understand FLO issues and choose FLO resources, then copyleft vs permissive licensing might become less of an issue.

### Social norms

**Licenses can act as social statements as well as legal terms**. For example, the licenses in the GNU family include a preamble that explains the intentions and values of software freedom. Some people point out that licenses are only practically enforceable if you have the means and willingness to follow through with legal action against infringers. However, the terms of a license tend to be followed by most people essentially as a social agreement. Thus, license choice matters even when no legal challenge ever occurs.

Of course, one may *ask* for certain behavior without using the terms of a legal license. Some people favor Copy*free* licenses for being the least legalistic, least restricted, and most compatible. A copyfree project could still make a public statement *requesting* that all derivatives keep the same terms and that all source files be published.

### License changes

Though rarely necessary, **changing a license can be complex**. FLO licenses are essentially irrevocable once someone publishes a product. New versions may get different licenses, but any original licensee can continue distributing the earlier version or derivatives of it under the original license. With copyleft licenses and contributions from many legal rights-holders, changing a license may require time-consuming work in achieving consensus from all parties.[^Relicense] Of course, works using one license can be combined with new updates in a compatible license, effectively licensing the package under the terms of the second license; however, the parts will retain their respective licenses.

[^Relicense]: With work, changes still remain feasible in most cases. For example, the Mozilla Foundation successfully changed all their projects from MPL 1.1 to MPL 2.0, despite lacking an "*or later version*" clause in the original license.

### Dual- or mixed-licensing

Sometimes a work gets released under multiple licenses. Although multiple licensing may maximize compatibility, it can also lead to incompatible derivatives if the set of licenses is not fully retained in all cases. We generally discourage careless dual-licensing. There should be a good reason for such decisions.

Proprietary businesses often offer lesser versions of a work under a FLO license while advanced features remain proprietary. We do not support that form of mixed licensing as all projects at Snowdrift.coop must be fully FLO.[^freemium]

In other cases, a project produces only FLO works but sells exceptions to their copyleft licensing, allowing other entities to make proprietary derivatives. Although we will accept such projects (requiring that they transparently report income from license-exception sales), we discourage the practice because it creates a conflict-of-interest where the project benefits from encouraging proprietary development.[^Stallman]

[^Stallman]: Richard Stallman sees selling license exceptions as no worse than permissive licenses themselves, see <https://www.fsf.org/blogs/rms/selling-exceptions>. While his points are logical, they ignore the significant conflict-of-interest that arises from this practice.


### Opinions in the FLO community

Many people hold strong feelings about licensing issues. Some of those who care greatly about FLO issues prefer to contribute only to copyleft projects; they wish never to end up helping proprietary products. Other FLO supporters have various ideological and practical complaints about copyleft and so will contribute more readily to permissively licensed projects.[^Attitudes]

[^Attitudes]: For more discussion on license attitudes, see [*A Field Guide To Copyleft Perspectives*](http://dustycloud.org/blog/field-guide-to-copyleft/).

Copyleft supporters also differ in their views about license details. Linus Torvalds, the namesake and primary lead developer of Linux, prefers the GNU GPLv2 over later versions. Torvalds appreciates the way copyleft requirements assist him in accessing the code from derivative works. However, he does not like how GPLv3 closes some GPLv2 loopholes because those loopholes do not affect his ability to access the source, they only affect end-user freedoms — which Torvalds does not especially value.[^Tivoization]

[^Tivoization]: See <https://en.wikipedia.org/wiki/Tivoization>

Proprietary companies sometimes oppose the stronger copyleft licenses that limit their power and flexibility. For example,  Apple Computer builds its core products on FLO software, but strongly prefers permissive licenses and has worked to avoid using copyleft software.[^Apple] Also, companies like Google strongly oppose the strongest copyleft license, the AGPL (the license used for the Snowdrift.coop code), because it blocks their proprietary business model (not distributing software for others to run but only giving public access over a server, such as to Google Docs, Google Maps, Gmail web client etc.). With regular GPL, allowing access over a server does not count as conveyance and so the copyleft terms do not apply.[^Politics]

[^Apple]: See <http://meta.ath0.com/2012/02/05/apples-great-gpl-purge/> for example.

[^Politics]: For more perspective on the complex political issues around copyleft licenses, see Bradley Kuhn's talk [Considering the Future of Copyleft](https://www.youtube.com/watch?v=-ItFjEG3LaA)

#### New types of licenses and license proliferation

Some people want to discriminate against entities who they see as exploiting FLO works unjustly. For example, the Peer Production License (sometimes described as a copyfarleft license) basically says that the license is only for co-ops, non-profits, and individuals (the goal being to exclude traditional corporate capitalists). Other ideas include defensive license terms such as those that would revoke all rights to *any* works under a particular license for any entity that brought a lawsuit or other aggression against any FLO project.

These ideas may be based in good intentions, but they present lots of legal questions and compatibility problems with the existing pool of FLO works. We will not accept such novel license concepts at Snowdrift.coop until/unless they are accepted by entities like OSI and FSF.

### Proprietary projects transitioning to FLO

For proprietary projects wishing to transition to FLO, we plan to eventually offer an option where they can sign up and begin getting pledges from patrons. Such projects will then only receive funds after they meet all our [core requirements](project-reqs.md).

[^attribution]: Licenses may also use Trademark to protect a product's reliability and reputation, and we accept such clauses when included in FLO standards. For example, Debian must rebrand their modified Firefox as "IceWeasel" due to the trademark clause from the Mozilla Public License. *Note:* while some people confuse them, Trademark terms in licenses require *removal* of branding for derivatives (in order to protect the brand's reputation), while attribution is about *retaining* statements of credit for derivatives.

[^freemium]: In some particular cases, a business making their own original resources and/or with a Contributor License Agreement (CLA) might specifically use copyleft licenses for a lesser version of a product while selling *proprietary* licenses for a premium version. By doing so, they can invite community contributions and share the lesser version openly (thus building a user base and perhaps some good will which may increase their proprietary sales) while blocking *other* businesses from making fully competing proprietary derivatives. In these cases, the copyleft terms *still* work as intended and block proprietary terms; except because one business holds the copyright, they can give themselves an exception.

    Of course, this practice is anti-competitive, and any CLA is itself a type of proprietary copyright license, but the versions of such products published under a FLO license still remain FLO. So, while supporters of FLO values rightly criticize this form of "freemium" publishing, the problem comes not from the copyleft license itself but from the proprietary licenses involved. Some argue that encouraging this practice provides a net gain for FLO compared to the same projects publishing only proprietary versions, but concerns remain about conflicts of interest and confusion in these cases which may co-opt and otherwise undermine the promotion of FLO values. We see this as a complex issue to consider on a case-by-case basis, but Snowdrift.coop only directly supports fully-FLO projects, so we will not include any projects that use this sort of FLO/proprietary mixed scheme. We support only public goods, and that does not include projects that reserve features to the special club that buys the proprietary license.
