# Transparency Requirements for Projects

categories: governance, project-liaison
...

Patrons must be able to verify that projects are competent, reliable, and honest. Our transparency requirements ensure that projects efficiently demonstrate their trustworthiness to the community.

## General background and progress

* List all sources and summarize the amounts of other project funding including sales, support services, donations outside Snowdrift.coop, etc.

* List any paid team members and major outside contributors who aren't registered users at Snowdrift.coop (registered users are shown automatically when marked with their roles or thanked for contributions).

* Releases regular updates of work-in-progress in some public fashion, not just widely spaced final releases.

* Clearly state how decisions are made.
    * Consider questions like: How does the team decide what direction to go? How is it decided to include or not include certain things? Etc.
    * We *strongly suggest* public comment periods and other consensus-seeking methods.
    * Use public mailing lists, wikis, discussion boards, public IRC channels, etc for communication (both between team members and with patrons) wherever feasible so the community can understand the process.

* Be clear about what parts of the final product are your own work. Credit upstream projects and any other sources drawn upon (within reason) appropriately. When required by the license, indicate in the license statement what parts of the work have been modified and which are still in their original form.

* Report your project's status on [honor items](honor-projects.md) fully and honestly, and update if your status changes.

## Financial record-keeping

* **For [minimum requirements](project-reqs.md):** Keep project-internal records documenting the final allocation of funds and how that was determined (such as salaried team, billable hours, work contract, etc). These records need not be published but should be available to access if necessary. Projects that are officially incorporated will need to keep detailed records in accordance with legal requirements for their tax status.
    * **Honor Code preferred, but not absolutely required:**
        * *Better:* Create a general budget/business plan showing expected costs, estimated work required, and how the project intends to use donated money to meet those needs. Make your budget public. You don't need to show in advance exactly how much will be allocated to each team member, but it is good to indicate how that will be determined.
        * *Best:* Publicly list how much each team member receives and how that was calculated. *Note: Snowdrift.coop does not advocate adhering to strict “billable hours” or other quantitative criteria. Some uses of time can be questionable, and some hours are better or more project-focused than others. We leave it up to each project team to determine how to evaluate quality and necessity of work and how to report their use of time.*

## Respecting confidentiality

Openness and accountability do not preclude confidentiality when appropriate. Personal privacy should be respected. Private decision processes or other private discussions are warranted in cases such as disciplinary matters.
