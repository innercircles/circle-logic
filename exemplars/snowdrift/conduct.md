# Snowdrift.coop Code of Conduct

categories: communication, conduct
...


# Snowdrift.coop Code of Conduct

Version 2.0

This Code of Conduct applies to all spaces within and related to Snowdrift.coop including our forum, IRC/Matrix channel(s), live meetups, and elsewhere.

**Purpose**

We built our Code of Conduct with the aims of:

- maintaining a healthy and diverse community, free from abuse and harrassment
- reducing and resolving conflicts and misunderstandings
- keeping the community resilient in light of any attacks from malicious actors

**Conduct enforcement principles**

Our [conduct enforcement procedures](conduct-enforcement.md) aim to avoid community disruptions from conduct violations or their resolution process. In addressing any violation, our first priority is to minimize further harms to individuals or the community.

Anyone who sees harmful behavior has the *responsibility* to take action (such as flagging or reporting). We invite those involved in a violation to participate in a resolution process away from regular community spaces. For example, flagging a forum post sends a private, anonymous message to the author prompting them to edit the post. If that resolves the problem(s), great.

As needed, we have other moderation and conflict-resolution procedures. When feasible, participants can decide by consensus whether to work on resolution privately or publicly, with or without facilitation. We only restrict or exclude anyone in the unlikely case of a participant causing persistent problems and showing inadequate willingness or ability to improve.

**The conduct rules**

To participate in the Snowdrift.coop community, you must accept the following pledges and descriptions which comprise our enforced conduct rules:

## I will participate with honesty and good faith

*Though not a formal part of the Code of Conduct,  we have compiled further [guidelines for healthy communication](honor-users.md). Consider revisiting those regularly.*

## I will honor our community values

Participants here should understand and follow our [Guiding Values and Principles](values.md). They include co-op values and ethics, support for free/libre/open movements, and celebration of diversity.

For example, this means **no bigotry** — no stereotyping of or promoting prejudice against particular groups or classes of people. In cases where constructive criticism of ideology or culture remains on-topic, respectfully discuss the *ideas* rather than the people who hold them.

Our diversity emphasis also means consideration for special needs. For example, avoid posting flashing images that could endanger those with photosensitive epilepsy (or at least use our forum's "hide details" function with an appropriate warning in the description).

## I will treat others with dignity and compassion

**No harassment, ad hominem attacks, threats, intimidation, or other hostile behavior.**

Do not be condescending, demeaning, derogatory, disdaining, or insulting.

## I will respect others' privacy

**No stalking or other unwelcome personal attention. No revealing or speculating about personal details of others.**

Only publish private communications with the consent of everyone involved.

Unless you have consent from all related parties, only publicly reference a conduct violation or its resolution with good reason and with omission of any details that would identify participants.

In cases of good-faith curiosity about someone's experience or identity, ask politely in a manner that they will feel free to decline.

## I will criticize constructively

Keep criticism specific and helpful. Criticize actions or ideas, not people.

Instead of simply saying that something is bad or doesn't work, provide details about the problem, and offer suggestions if possible.

### I will take constructive criticism gracefully

When others respectfully and constructively criticize your views or work (but not you personally), respond respectfully and constructively. Do not take differences of opinion personally. Either engage respectfully or agree to disagree.

## I will avoid patterns that escalate conflicts or promote hysteria

Surveillance capitalists (Facebook, Twitter, and so on) exploit people's susceptibility to panic and outrage in order to maximize engagement (albeit, unhealthy engagement) with their ad-driven platforms.

Don't enable such patterns. Don't spread FUD (fear, uncertainty, and doubt) or negative rumors. Don't cast vague aspersions about projects, the community, or specific people. Avoid baiting or provoking others.

## I will respect discussion topics and not derail conversations

In focused contexts (such as at our forum, on issue tickets, etc.), stay on topic.

To discuss a tangent or make a meta comment about the conversation, use a new topic or private message. Our forum has a convenient "reply as linked topic" option, and we have a specific "meta" category for discussing discussions. For side-notes that don't need to become their own topics, use the "hide details" or footnote features. In topical cases outside the forum, you can share a link to where off-topic discussion will continue.

If you see a violation of this Code of Conduct, follow our [conduct enforcement procedures](conduct-enforcement.md). Do not bring up allegations within regular discussion.

### No dogpiling

"Dogpiling" is when a large *quantity* of people of the same perspective reply with effectively redundant posts, thus overwhelming a discussion. When others have adequately expressed a view you support, stick to less intrusive tools like our forum's "appreciate" reaction button instead of posting additional replies.

### No spamming

Never post completely off-topic items or repeated links or statements outside of appropriate context.

In appropriate contexts, honest advertising may be okay if the products, services, movements, or ideas are relevant to Snowdrift.coop, aligned with our values, and you disclose any connections you have with whatever is being advertised.

### No sexual content

Sexual references can make people feel uncomfortable and are off-topic for anything relevant to Snowdrift.coop. At most, some exceptions may include meta issues such as discussing sexism or discussing this policy itself.

## I will fix my mistakes

Anyone who recognizes a problem with their own actions must take initiative to rectify the situation (asking for guidance or other assistance as needed).

If *your* comment is flagged or otherwise called out and you need help or feel a flagging is *wrong* or *malicious*, alert a moderator.

## I will help others fix their mistakes

Minor problems commonly come from misunderstandings, error, ignorance, or carelessness rather than from bad faith. Our [conduct enforcement procedures](conduct-enforcement.md) emphasize self-help, restorative justice, and conflict resolution; and we have procedures for all sorts of cases — not just the most serious violations.

**Take action any time you see even minor violations of this Code of Conduct**, whether or not you feel personally offended or were involved in the discussion. Of course, any feedback or suggestions about problem posts or behavior must also follow the Code of Conduct.

---

Further reading (some of these are linked above as well):

- [conduct enforcement procedures](conduct-enforcement.md) — details on handling violations and related
- our guiding [values and principles](values.md)
- [healthy communication guide](honor-users.md)