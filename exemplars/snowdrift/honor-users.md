# Honor Code for General Users

categories: community, honor
...

## Understand the challenges in online discussion

We can't simply ask everyone to always post ideal, tactful, respectful, and clear messages. Maintaining healthy online discourse with diverse perspectives requires care in how we react to messages we dislike, disagree with, or don't understand.

When we react with aggression or defensiveness, it often leads to negative feedback loops. When we react by ignoring or voting-down, that censoring of unpopular ideas can lead to echo chambers and groupthink.

## Promote and teach respectful, effective communication

At Snowdrift.coop, we welcome diverse perspectives. Because expressing controversial or unpopular views can be especially difficult, it's especially important for everyone to help one another communicate well, even when we do not agree.

We can all enhance our compassion, open-mindedness, and understanding through the practice of expressing ourselves thoughtfully and respectfully and through assisting others in doing the same.

### Giving the chance to improve

Any problematic posting is as a chance to improve the communication patterns. We
encourage everyone to help one another in communicating effectively and
thoughtfully.

Even more serious offenses are treated with Restorative Justice in mind. That
means working together (with moderation as needed) to grow and forgive with the
perspective of those who feel wronged being central to determining the best
resolutions.

See our [conduct enforcement](conduct-enforcement.md) page for more details about
how we manage violations and conflicts.

## Honor ideals

While we only take enforcement measures against serious violations, honorable participation also involves commitment to the following *ideals*:

### Do your part

Be a patron, a contributor, an advocate, a constructive critic, and an otherwise engaged supporter of various projects. While you won't likely engage with each and every project you like, you can make a reasonable effort to bring value to the community overall.

#### Hold projects accountable

Give reasonable benefit-of-the-doubt to both projects and to other users; but hold projects accountable, point out concerns, encourage projects to stay honorable and meet their commitments, and give support to projects that show consistent good will and honor.

#### But allow discretion

Encourage and respect the creative autonomy of project teams in determining priorities. Make polite requests, not demands.

### Strive for ideals but avoid dogmatism

Recognize our community's [vision](https://wiki.snowdrift.coop/governance/mission#vision) of a prosperous, fair, ethical world; but use good judgment to make practical decisions on a case-by-case basis. In short: use practical wisdom over simplistic dogma or rigid zero-tolerance rules.

###  Maintain respectful and effective communication

#### Be positive, friendly, and considerate

Give others the benefit of the doubt. Assume good faith. Thank others, pointing out specific things you appreciate.

#### Participate in an authentic and active way

Present yourself honestly, emphasizing transparency and integrity. Make liberal use of emoticons and other casual, positive conversation style. Respect anyone's decision to protect their privacy and remain anonymous, but encourage personal, human interaction.

### Be effective and concise

* Maximize the signal-to-noise ratio by reducing the bulk of unnecessary comments.
* Aim for short sentences.
* Provide links.
* Avoid duplicating existing items.
* Use tags and other tools to avoid posting extraneous reply comments.
* Use our [IRC channel](https://wiki.snowdrift.coop/community/irc) for more open-ended casual discussion.

### Use empirical evidence and concrete examples

Provide concrete empirical examples, links, and citations whenever feasible.

### Welcome newcomers

* Avoid assumptions about others' knowledge or views.
* Help educate newcomers about how to best use the resources, how the system works, and encourage them to learn and care about the community's ideals.
* Answer questions with patience and grace, no matter how basic.

### Use best-practice communication techniques

* Recognize the strong tendency for misunderstandings of pure text (especially the difficulty in discerning jokes, sarcasm, and other facetiousness).
* Ask for clarification before judging others' intentions.
* Favor first-person perspectives describing your views rather than second-person "you statements".
* Do not make personal issues out of intellectual differences.
* Embrace chances to learn and change your views.
* Welcome others' change of mind gracefully without saying "I told you so."
* Whether reacting positively or negatively, express meaningful judgments of statements, actions, and effects rather than judgments of people.
    * E.g. better to say "that was a wonderful explanation!" than "you're so good at explaining things!"
* Avoid carrying disagreements from one topic to another. Treat each new discussion with a fresh perspective.
* Welcome diverse perspectives and identities, and avoid assumptions about others' identities, views, or experiences.
* Respectfully point out [rhetorical fallacies](https://en.wikipedia.org/wiki/List_of_fallacies), avoid them yourself, and do not take a mention of fallacies as a personal attack.
* Ask about the accuracy of your impressions of others' views. Don't put words in others' mouths.
* Avoid reacting to specific words. Instead, help others clearly express their thoughts even if you do not agree with their views.

* Consider [Active Listening](https://en.wikipedia.org/wiki/Active_listening) concepts and the following *guidelines* from Anatol Rapoport (by way of Dan Dennett):

    > ##### Rapoport's Rules
    > 1. Attempt to re-express the other person’s position so clearly, vividly, and fairly that they say, 'Thanks, I wish I’d thought of putting it that way.'
    > 2. List any points of agreement (especially if they are not matters of general or widespread agreement)
    > 3. Mention anything you have learned from them
    > 4. Only now are you permitted to say so much as a word of rebuttal or criticism.

    * A similar framing: Don't use straw-man (or even weak-man) arguments (critiquing *low-quality* versions of opposing views). Instead, present ***steel-man*** (improved, stronger) versions of others' views before criticizing them.

* Acknowledge respectfully-presented constructive criticism.
* Be forgiving; do not hold grudges against others for past violations.

### Educate others

We encourage consciousness-raising. Values and honor matter. Do your part to educate others. Thank others who are working to do consciousness-raising (or at *least* tolerate them and do not complain about their efforts).
