<div align="center">

[![Snowdrift Coop](https://codeberg.org/innercircles/circle-logic/raw/branch/main/exemplars/snowdrift/snowdrift-logo_symbol-wordmark_dark-blue.png)](https://snowdrift.coop)

# [Crowdmatching for public goods](https://snowdrift.coop)

</div>

## Snowdrift Code of Conduct

The Snowdrift Code of Conduct and all its related documents are published on their [Wiki](https://wiki.snowdrift.coop/) and are available for reuse and modification under an [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) license. The documents below were obtained from the wiki and relate to Version 2.0 of the Code of Conduct.

> **Note**: Modifications were made to fix titles, links, footnotes and formatting.

- [Snowdrift.coop Code of Conduct](conduct.md)
- [Snowdrift.coop Conduct Enforcement Procedures](conduct-enforcement.md)
- [Snowdrift.coop Values and Principles](values.md)
- [Snowdrift.coop Honor Code for General Users](honor-users.md)
- [Snowdrift.coop Honor Code for Projects](honor-projects.md)
- [Snowdrift.coop Project Requirements](project-reqs.md)
- [Snowdrift.coop Transparency Requirements](transparency.md)
- [Snowdrift.coop What Does *Free/Libre/Open* Mean?](free-libre-open.md)
- [Snowdrift.coop Why FLO?](why-flo.md)
- [Snowdrift.coop FLO Licensing Discussion](licenses.md)

#### Overall impression

The wiki is a treasure to find out exactly how Snowdrift positions itself, and what they find important. It is very refreshing to see a company (cooperative) do this to this extent and so transparently. It represents a great way to build a trust relationship with the users of the platform.

#### Code of Conduct

The code of conduct uses friendly easy-to-understand language:

- Conduct rules have very descriptive, short titles.
- Conduct rules start with "I will..".
- Descriptions are short and to the point.

Where it comes to 'honoring community values' the CoC fans out to:

- [Guidelines for healthy communication](honor-users.md) (not formally part of the CoC)
- [Guiding values and principles](values.md)]
- [Honorabale project ideals](honor-projects.md)]

From these document there is further fan-out. The texts here are probably TL;DR for most people and are only brought into play as reference when conflicts arise.

The [Conduct enforcement procedures](conduct-enforcement.md)] describes how conduct violations are handled.