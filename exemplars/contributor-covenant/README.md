# The Contributor Covenant

Created in 2014 by [Coraline Ada Ehmke](https://where.coraline.codes/) in 2014, the [Contributor Covenant](https://www.contributor-covenant.org/) is the best-known, most widely used way to specify Code of Conduct for open source projects. The covenant is available under a [Creative Commons Attribution 4.0 International (CC-BY-4.0)](https://github.com/ContributorCovenant/contributor_covenant/blob/release/LICENSE.md) license, and available on [Github](https://github.com/ContributorCovenant/contributor_covenant).

## Contributor Covenant Code of Conduct

Here is the latest version of the covenant:

- [Contributor Covenant v2.0](contributor-covenant.md)

#### Structure

The document is not overly long, and is very clearly worded. It defines both desired conduct, as well as enforcement procedures. Here is the structure:

- Our pledge
- Our standards
- Enforcement Responsibilities
- Scope
- Enforcement
- Enforcement Guidelines
    1. Correction
    2. Warning
    3. Temporary Ban
    4. Permanent Ban
- Attribution