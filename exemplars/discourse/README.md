<div align="center">

[![Discourse](https://codeberg.org/innercircles/circle-logic/raw/branch/main/exemplars/discourse/discourse-logo.png)](https://www.discourse.org/)

# [Civilized discussion for your community](https://www.discourse.org/)

</div>

## Discourse FAQ

The Discourse [FAQ](faq.md), other than the name implies, is actually the Code of Conduct for users to the forum software.

## Discourse Terms of Service

The Discourse [Terms of Service](tos.md) provide a more formal, legal basis in additionn to the Code of Conduct. The document is included in the exemplars, because it is tailored to forum use, and therefore interesting.